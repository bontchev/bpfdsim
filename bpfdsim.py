#!/usr/bin/env python

from __future__ import print_function

from argparse import ArgumentParser
from binascii import hexlify
from socket import inet_ntoa
from struct import pack, unpack
from time import localtime, strftime, time

try:
    from twisted.internet import reactor
    from twisted.internet.protocol import DatagramProtocol, Factory, Protocol
except ImportError:
    print('Could not import Twisted; try "pip install Twisted".')
    exit()


__description__ = 'BPFDoor simulator'
__license__ = 'GPL'
__uri__ = 'https://gitlab.com/bontchev/bpfdsim'
__VERSION__ = '1.0.0'
__author__ = 'Vesselin Bontchev'
__email__ = 'vbontchev@yahoo.com'


class BackdoorUDPSender(DatagramProtocol):
    def __init__(self, payload, host, port):
        self.payload = payload
        self.addr = (host, port)

    def startProtocol(self):
        # Called when transport is connected
        self.transport.write(self.payload, self.addr)
        self.transport.stopListening()


def process_request(data, protocol, answerport, verbose):
    def formatted_hex(data, spaced=True):
        hex_data = hexlify(data).upper().decode('ascii')
        if spaced:
            return ' '.join(hex_data[i:i+2] for i in range(0, len(hex_data), 2))
        else:
            return hex_data

    if verbose:
        now = localtime(time())
        timeStr = str(strftime('%Y-%m-%d %H:%M:%S', now))
        print('[{}] Received:'.format(timeStr))
        print(formatted_hex(data))
    # Check if data is an "are you there packet"
    if len(data) != 24:
        return
    header = data[0:4]
    proper_header = b'\x72\x55\x00\x00' if protocol == 'UDP' else b'\x52\x93\x00\x00'
    if header != proper_header:
        if verbose:
            print('Wrong {} header: {}.'.format(protocol, formatted_hex(header, False)))
        return
    ip = inet_ntoa(data[4:8])
    port = unpack('>H', data[8:10])[0]
    if verbose:
        print('{} request from {}:{}.'.format(protocol, ip, port))
    reactor.listenUDP(answerport, BackdoorUDPSender(b'1', ip, port))


class BackdoorUDPListener(DatagramProtocol):
    def __init__(self, answerport, verbose):
        self.answerport = answerport
        self.verbose = verbose

    def datagramReceived(self, data, addr):
        process_request(data, 'UDP', self.answerport, self.verbose)


class BackdoorTCPListener(Protocol):
    def __init__(self, answerport, verbose):
        self.answerport = answerport
        self.verbose = verbose

    def dataReceived(self, data):
        process_request(data, 'TCP', self.answerport, self.verbose)


class TCPFactory(Factory):
    def __init__(self, answerport, verbose):
        self.answerport = answerport
        self.verbose = verbose

    def buildProtocol(self, addr):
        return BackdoorTCPListener(self.answerport, self.verbose)


def get_options():
    parser = ArgumentParser(description=__description__)
    parser.add_argument('-v', '--version', action='version',
                        version='%(prog)s version {}'.format(__VERSION__))
    parser.add_argument('-a', '--answerport', type=int, default=9999,
                        help='UDP port to answer to (default: %(default)s)')
    parser.add_argument('-u', '--udpport', type=int, default=9998,
                        help='UDP port to listen to (default: %(default)s)')
    parser.add_argument('-p', '--tcpport', type=int, default=9997,
                        help='TCP port to listen to (default: %(default)s)')
    parser.add_argument('-b', '--verbose', action='store_true',
                        help='Print what is being received')
    args = parser.parse_args()

    if args.answerport == args.udpport:
        print('"anwerport" must not be the same as "udpport".')
        exit()
    elif args.answerport == args.tcpport:
        print('"anwerport" must not be the same as "tcpport".')
        exit()
    elif args.udpport == args.tcpport:
        print('"udpport" must not be the same as "tcpport".')
        exit()

    return args


def main():
    args = get_options()

    if args.verbose:
        print('Starting server on TCP port {} and UDP port {}...'.format(args.tcpport, args.udpport))

    reactor.listenUDP(args.udpport, BackdoorUDPListener(args.answerport, args.verbose))
    reactor.listenTCP(args.tcpport, TCPFactory(args.answerport, args.verbose))
    reactor.run()

    if args.verbose:
        print('Shutdown requested, exiting...')


if __name__ == '__main__':
    main()
