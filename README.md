# bpfdsim

A BPFDoor simulator

## Description

This program is a primitive simulator of a remote machine infected with the
BPFDoor backdoor.

BPFDoor is a backdoor used by Chinese state-sponsored hackers during the
post-exploitation process. Its purpose is to ensure that the attackers
can re-gain access to the initially compromised machine, if their presence
there is discovered and removed.

For a detailed technical description of how BPFDoor works, see
[this excellent report](https://www.sandflysecurity.com/blog/bpfdoor-an-evasive-linux-backdoor-technical-analysis/)
from the Sandfly Security Team. For a general overview, see
[Kevin Beaumont's blog post](https://doublepulsar.com/bpfdoor-an-active-chinese-global-surveillance-tool-54b078f1a896).
Finally, for a more accessible description (largely based on Sandfly's
report), see
[this Bleeping Computer article](https://www.bleepingcomputer.com/news/security/bpfdoor-stealthy-linux-malware-bypasses-firewalls-for-remote-access/).

In short, BPFDoor uses the Extended Berkeley Packet Filter technology to
install a packet filter on the compromised machine. This filter allows it
to listen and communicate through any open TCP or UDP port on the machine
(or even via ICMP), even if the port is already in use by another (legitimate)
program, by essentially performing a man-in-the-middle attack and intervening
between the legitimate program and the port.

Most communications accepted or performed by BPFDoor are encrypted with the
RC4 cipher and a password. However, curiously, the "are you there?" call is
not. If a special packet, containing a magic number header, an IP address,
and a port, is send to any open TCP or UDP port of the infected machine (or
via ICMP), BPFDoor will intercept it and will reply via UDP to the IP address
and port specified in the packet by sending the string `'1'` there. This makes
it possible to create a scanner for machines infected with BPFDoor without
having access to the particular instance of the backdoor that has infected
the machine and without having the password, necessary for the other kinds of
communications with the backdoor.

This simulator simulates a machine infected with BPFDoor and answering the
"are you there?" call (and nothing else), so that you'd be able to write
and test such a scanner for BPFDoor yourself. (I have implemented [one
such scanner](https://gitlab.com/bontchev/bpfdscan) myself and have released
it as an open-source project.)

As mentioned at the beginnig of this section, this is a very primitive
simulator. In particular, it has the following limitations, compraed to the
real thing:

* It implements only the "are you there call?". The original BPFDoor can
open a shell or a reverse shell on the infected machine on request.

* It listens only on *one* TCP port and *one* UDP port (specified by the
user) for the "are you there?" call - and these two ports must be different.
The original BPFDoor listens on all opened TCP and UDP ports of the
compromised machine. I cannot do this (and I cannot listen to both the TCP
and the UDP protocol on the same port) without implementing an eBPF just
like BPFDoor.

* The simulator does not handle the ICMP protocol, which BPFDoor supports.

## Installation

There is nothing special you need to do, in order to install this program;
it sould run out-of-the-box. The scipt is compatible with both Python 2.x
and Python 3.x.

The only external dependency that it has is the Twisted communications framework.
In the unlikely case that you don't have that installed already, you can
do so with the command

```bash
pip install twisted
```

The script listens to two ports (TCP 9997 and UDP 9998 by default) and
sends information via UDP port 9999 by default. Any of the three ports
can be changed with a command-line option but no two of them can be
the same number (the program will warn you and abort, if you break this
rule).

You should make sure that communications via the two listening ports can
reach the computer that is running the script and that communications
via the third port can reach the machines scanning it. If necessary,
create the relevant rules in your machine's firewall configuration to
open these ports for incoming and outgoing traffic.

If your machine is behind a NAT router, you'll have to forward these ports
at the router, too, and make sure that traffic through them from the Internet
can reach your machine.

## Usage

```bash
python bpfdsim.py [-h] [-v] [-a ANSWERPORT] [-u UDPPORT] [-p TCPPORT] [-b]
```

The script accepts the following command-line options:

`-h`, `--help` Displays a short explanation how to use the script and what
the command-line options are.

`-v`, `--version` Displays the version of the script and exits.

`-a`, `--answerport` The UDP port via which the simulator answers the "are
you there?" call by sending the string `'1'`. The default is 9999.

`-u`, `--udpport` The UDP port on which the simulator listens for the "are
you there?" call. The default is 9998.

`-p`, `--tcpport` The TCP port on which the simulator listens for the "are
you there?" call. The default is 9997.

`-b`, `--verbose` Makes the simulator print to `stdout` which TCP and UDP
ports it is listening to, as well as what information it has received from
there. By default the simulator is completely silent.

## License

This project is licensed under the GNU v3.0 license.
